from unittest import TestCase

from django.db.models import Sum

from bookings.models import Table


class TestTables(TestCase):
    def setUp(self) -> None:
        Table.objects.all().delete()
        pass

    def test_available_tables(self):
        Table.objects.create()
        Table.objects.create(number_of_seats=4)
        seat_count = Table.objects.aggregate(Sum("number_of_seats")).get('number_of_seats__sum')
        self.assertEqual(seat_count, 6)
