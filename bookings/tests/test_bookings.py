from dateutil import parser
from unittest import TestCase

from bookings.availability import free_seats
from bookings.models import *


class TestBookings(TestCase):
    def setUp(self) -> None:
        Table.objects.all().delete()
        OpenDate.objects.all().delete()
        Booking.objects.all().delete()
        Table.objects.create()
        Table.objects.create(number_of_seats=4)
        OpenDate.objects.create(date="2001-1-1")
        pass

    def test_available_seats_without_bookings(self):
        self.assertEqual(free_seats(parser.parse("2001-1-1 12:00")), 6)
        self.assertEqual(free_seats(parser.parse("2001-1-2 12:00")), 0)

    def test_available_seats_with_bookings(self):
        # TODO: Create a booking for 2001-01-01
        self.assertEqual(free_seats(parser.parse("2001-1-1 12:00")), 4)
