from django.db import models


class Table(models.Model):
    number_of_seats = models.IntegerField(default=2)


class OpenDate(models.Model):
    date = models.DateField()


class OpenTime(models.Model):
    class DayOfWeek(models.IntegerChoices):
        MONDAY = 1
        TUESDAY = 2
        WEDNESDAY = 3
        THURSDAY = 4
        FRIDAY = 5
        SATURDAY = 6
        SUNDAY = 7

    day_of_week = models.IntegerField(choices=DayOfWeek.choices)
    start_at = models.TimeField()
    end_at = models.TimeField()


class Booking(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    number_of_guests = models.IntegerField()
    date = models.DateField()
    time = models.TimeField()
