import datetime

from django.db.models import Sum

from bookings.models import *


def free_seats(at: datetime):
    at_date = at.date()
    at_time = at.time()
    date_count = OpenDate.objects.filter(date=at_date).count()  # 0 or 1
    if date_count == 0:
        return 0

    seat_count = Table.objects.aggregate(Sum("number_of_seats")).get('number_of_seats__sum')
    # TODO: add margin and upper limit
    booked_tables = (
        Booking.objects
        .select_related('table')
        .filter(table__booking__date=at_date, table__booking__time=at_time)
        .aggregate(seats=Sum("table__number_of_seats")).get("seats"))

    return seat_count - (booked_tables or 0)
